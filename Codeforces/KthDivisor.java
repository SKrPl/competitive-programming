// https://codeforces.com/contest/762/problem/A

import java.util.*;

public class KthDivisor {

    public static void main(String[] argv) {
        Scanner sc = new Scanner(System.in);
        long n = sc.nextLong(), k = sc.nextLong();
        List<Long> factors = getFactors(n);
        Collections.sort(factors);
        if (factors.size() < k) System.out.println(-1);
        else System.out.println(factors.get((int)k-1));
    }

    private static List<Long> getFactors(long n) {
        List<Long> factors = new ArrayList<>();
        long m = (long)Math.sqrt(n) + 1;
        for (long i = 1; i < m; i++) {
            if (n % i == 0) {
                factors.add(i);
                if (n/i != i) {
                    factors.add(n/i);
                }
            }
        }
        return factors;
    }
}
