// https://codeforces.com/problemset/problem/664/A

#include <stdio.h>
#include <string.h>

int main() {
	char a[110], b[110];
	scanf("%109s %109s", a, b);
	if (strcmp(a, b) == 0) printf("%s\n", a);
	else printf("1\n");
	return 0;
}
