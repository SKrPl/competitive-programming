// https://codeforces.com/problemset/problem/17/A

#include <stdio.h>
#include <string.h>

int main() {
	int n, k;
	scanf("%d %d", &n, &k);

	int ary[n+1];
	memset(ary, 0, (n+1)*sizeof(int)); // 0 means prime
	ary[0] = ary[1] = -1;
	int i, j;
	for (i = 2; i*i <= n; i++) {
		if (ary[i] == 0) {
			for (j = i*i; j <= n; j += i) {
				ary[j] = -1;
			}
		}
	}
	
	int kcount = 0;
	int tmp = 0, noldbach = 0;
	for (i = 2; i <= n; i++) {
		if (ary[i] == 0) {
			tmp++;
			noldbach += i;
			if (tmp == 2) {
				if (noldbach+1 <= n) {
					if (ary[noldbach+1] == 0) kcount++;
				}
				noldbach = i;
				tmp = 1;
			}
		}
	}
	char *ans = kcount >= k ? "YES" : "NO";
	printf("%s\n", ans);
	return 0;
}
