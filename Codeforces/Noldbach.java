// https://codeforces.com/problemset/problem/17/A

import java.util.Scanner;
import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;

public class Noldbach {

    public static void main(String[] argv) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(), k = sc.nextInt();
        boolean[] ary = new boolean[n+1];
        List<Integer> primes = getPrimes(n, ary);
        int count = 0;
        for (int i = 1; i < primes.size(); i++) {
            int tmp = primes.get(i) + primes.get(i-1) + 1;
            if (tmp <= n && ary[tmp]) count++;
        }
        String ans = count >= k ? "YES" : "NO";
        System.out.println(ans);
        sc.close();
    }

    private static List<Integer> getPrimes(int n, boolean[] ary) {
        Arrays.fill(ary, true);
        ary[0] = ary[1] = false;
        for (int i = 2; i * i <= n; i++) {
            if (ary[i]) {
                for (int j = i*i; j <= n; j += i) {
                    ary[j] = false;
                }
            }
        }
        
        List<Integer> primes = new ArrayList<>();
        for (int i = 2; i <= n; i++) {
            if (ary[i]) primes.add(i);
        }
        return primes;
    }
}
