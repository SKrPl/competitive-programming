# https://codeforces.com/problemset/problem/230/B

def sieve(n: int) -> list:
    ary = [True]*(n+1)
    ary[0] = ary[1] = False

    for i in range(4, n+1, 2):
        ary[i] = False

    m = int(n**0.5) + 1
    for i in range(3, m, 2):
        if ary[i]:
            for j in range(i*i, n+1, i):
                ary[j] = False
    return ary

n = int(input())
ary = list(map(int, input().strip().split()))
prime_bool = sieve(int(max(ary)**0.5))
for i in ary:
    if i**0.5 - int(i**0.5) == 0:
        if prime_bool[int(i**0.5)]: print("YES")
        else: print("NO")
    else:
        print("NO")
