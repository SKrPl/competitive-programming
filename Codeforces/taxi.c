// https://codeforces.com/problemset/problem/158/B

#include <stdio.h>
#include <string.h>
#include <math.h>

int main() {
	int n;
	scanf("%d", &n);

	int strength[5], value, i;
	memset(strength, 0, 5*sizeof(int));
	for (i = 0; i < n; i++) {
		scanf("%d", &value);
		strength[value]++;
	}

	int taxis = 0;
	taxis += strength[4] + strength[3];
	taxis += strength[2]/2 + strength[2]%2;

	strength[1] = strength[1] - strength[3];
	strength[1] = strength[1] < 0 ? 0 : strength[1];
	if (strength[1]) {
		if (strength[2]%2) strength[1] -= 2;
		taxis += ceil((float)strength[1]/4);
	}

	printf("%d\n", taxis);
	return 0;
}
