// https://codeforces.com/problemset/problem/588/A

#include <stdio.h>

int main() {
	int n;
	scanf("%d", &n);
	int amount, price, i;
	
	int min_price = 1e7, cost = 0;
	for (i = 0; i < n; i++) {
		scanf("%d %d", &amount, &price);
		min_price = price < min_price ? price : min_price;
		cost = cost + min_price * amount;
	}
	printf("%d\n", cost);
	return 0;
}
