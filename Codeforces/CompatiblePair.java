import java.io.*;
import java.util.*;

public class CompatiblePair {

    private static InputStreamReader isr = new InputStreamReader(System.in);
    private static BufferedReader br = new BufferedReader(isr);
    private static Reader in = new Reader(br);

    public static void main(String argv[]) throws IOException {
        int[] nm = in.getIntArray();
        int[] tommy = in.getIntArray();
        int[] banban = in.getIntArray();

        Arrays.sort(banban);
        Arrays.sort(tommy);
        long t = tommy[nm[0]-2];
        long b = banban[nm[1]-1];
        System.out.println(t*b);
    }
}

class Reader {

    private BufferedReader br;

    public Reader(BufferedReader br) {
        this.br = br;
    }

    public int[] getIntArray() throws IOException {
        String[] tmp = br.readLine().split(" ");
        int ary[] = new int[tmp.length];
        for (int i = 0; i < tmp.length; i++) ary[i] = Integer.parseInt(tmp[i]);
        return ary;
    }
}
