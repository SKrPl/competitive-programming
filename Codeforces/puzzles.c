// https://codeforces.com/problemset/problem/337/A

#include <stdio.h>
#include <stdlib.h>

int comp(const void *a, const void *b) {
	return *(int*)a - *(int*)b;
}

int main() {
	int n, m;
	scanf("%d %d", &n, &m);
	int pieces[m], i;
	for (i = 0; i < m; i++) scanf("%d", &pieces[i]);

	qsort(pieces, m, sizeof(int), comp);
	int min = 1e5;
	for (i = n-1; i < m; i++) {
		int value = abs(pieces[i] - pieces[i+1-n]);
		min = value < min ? value : min;
	}
	printf("%d\n", min);
	return 0;
}
