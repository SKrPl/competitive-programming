// https://codeforces.com/problemset/problem/1041/A

#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b) {
	return *(int*)a - *(int*)b;
}

int main() {
	int n;
	scanf("%d", &n);
	int ary[n], i;
	for (i = 0; i < n; i++) scanf("%d", &ary[i]);

	qsort(ary, n, sizeof(int), compare);
	int count = 0;
	for (i = 1; i < n; i++) {
		count += ary[i] - ary[i-1] - 1;
	}
	printf("%d\n", count);
	return 0;
}
