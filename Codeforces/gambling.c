#include <stdio.h>
#include <stdlib.h>

int compare(const void *a, const void *b) {
	int x = *(int*)a, y = *(int*)b;
	if (x < y) return 1;
	else if (x == y) return 0;
	else return -1;
}

int main() {
	int n;
	scanf("%d", &n);
	int a[n], b[n], i;
	for (i = 0; i < n; i++) scanf("%d", &a[i]);
	for (i = 0; i < n; i++) scanf("%d", &b[i]);
	qsort(a, n, sizeof(int), compare);
	qsort(b, n, sizeof(int), compare);
	return 0;
}
