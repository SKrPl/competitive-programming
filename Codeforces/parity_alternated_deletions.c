// https://codeforces.com/problemset/problem/1144/B

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int compare(const void *a, const void *b) {
	return *(int*)a - *(int*)b;
}

int main() {
	int n;
	scanf("%d", &n);

	int even[n], odd[n];
	memset(even, 0, n*sizeof(int));
	memset(odd, 0, n*sizeof(int));
	int number, i, j = 0, k = 0;
	for (i = 0; i < n; i++) {
		scanf("%d", &number);
		if (number % 2) odd[j++] = number;
		else even[k++] = number;
	}

	int diff = abs(j - k);
	if (diff <= 1) {
		printf("0\n");
	} else {
		int sum = 0;
		if (j > k) {
			qsort(odd, j, sizeof(int), compare);
			for (i = 0; i < diff-1; i++) sum += odd[i];
		} else if (j < k) {
			qsort(even, k, sizeof(int), compare);
			for (i = 0; i < diff-1; i++) sum += even[i];
		}
		printf("%d\n", sum);
	}

	return 0;
}

