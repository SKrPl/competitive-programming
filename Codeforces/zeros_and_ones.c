// https://codeforces.com/problemset/problem/556/A
#include <stdio.h>
#include <stdlib.h>

int main() {
	int n;
	scanf("%d", &n);
	char string[n];
	scanf("%200009s", string);
	int zeros = 0, ones = 0, i;
	for (i = 0; i < n; i++) {
		if (string[i] == '0') zeros++;
		else ones++;
	}
	printf("%d\n", abs(zeros-ones));
	return 0;
}
