#include <stdio.h>

int gcd(int a, int b) {
	while (b > 0) {
		int tmp = a%b;
		a = b;
		b = tmp;
	}
	return a;
}

int main() {
	int t;
	scanf("%d", &t);
	while (t--) {
		int a, b;
		scanf("%d %d", &a, &b);
		int hcf = gcd(a, b);
		long int lcm = ((long int)a * (long int)b)/hcf;
		printf("%d %ld\n", hcf, lcm);
	}
	return 0;
}
